import tkinter as tk
from tkinter import font, messagebox

app = tk.Tk()
app.title("Fagyi kedvezmény számoló")
bg_color = '#2e2e2e'
fg_color = '#d9d9d9'
button_bg = '#4e4e4e'
app.configure(bg=bg_color)
screen_width = app.winfo_screenwidth()
screen_height = app.winfo_screenheight()
app_width = 330
app_height = 250
x_position = (screen_width - app_width) // 2
y_position = (screen_height - app_height) // 2
app.geometry(f"{app_width}x{app_height}+{x_position}+{y_position}")

custom_font = font.Font(family="Ariel Rounded MT Félkövér", size=10)

#gombóc ár alap beállítás
gomboc_price = tk.IntVar()
gomboc_price.set(450)

#minden 4. gombóc ajándék alap beállítás
free_gomboc_interval = tk.IntVar()
free_gomboc_interval.set(4)


def open_settings_window():
    global gomboc_price_entry, free_gomboc_interval_entry
    settings_window = tk.Toplevel(app)
    settings_window.configure(bg=bg_color)
    settings_window.title("Settings")
    settings_window.grab_set()
    settings_window.geometry("300x200")

    label1 = tk.Label(settings_window, text="Gombóc ára:", font=custom_font, bg=bg_color, fg=fg_color)
    label1.pack(pady=10)

    gomboc_price_entry = tk.Entry(settings_window, font=custom_font, width=8, justify='center')
    gomboc_price_entry.pack()
    gomboc_price_entry.insert(tk.END, gomboc_price.get())
    gomboc_price_entry.focus()

    label2 = tk.Label(settings_window, text="Melyik gombóc legyen ajándék?", font=custom_font, bg=bg_color, fg=fg_color)
    label2.pack(pady=10)

    free_gomboc_interval_entry = tk.Entry(settings_window, font=custom_font, width=8, justify='center')
    free_gomboc_interval_entry.pack()
    free_gomboc_interval_entry.insert(tk.END, free_gomboc_interval.get())

    close_button = tk.Button(settings_window, text="Save", command=lambda: close_settings_window(settings_window), font=custom_font, bg=button_bg, fg=fg_color)
    close_button.pack(pady=10)

    settings_window.bind('<Return>', lambda event: close_settings_window(settings_window))

    settings_window.update_idletasks()
    x_position = (app.winfo_width() - settings_window.winfo_width()) // 2
    y_position = (app.winfo_height() - settings_window.winfo_height()) // 2
    settings_window.geometry(f"+{app.winfo_x() + x_position}+{app.winfo_y() + y_position}")


def open_about_window():
    about_window = tk.Toplevel(app)
    about_window.title("About")
    about_window.geometry("300x150")
    about_window.configure(bg=bg_color)
    about_window.grab_set()

    label = tk.Label(about_window, text="Fagyi kedvezmény számoló v0.6\n Tamas \n 2023-09-04", font=custom_font, bg=bg_color, fg=fg_color)
    label.pack(padx=10, pady=10)

    # A középre igazítás a főablakhoz képest
    about_window.update_idletasks()
    x_position = (app.winfo_width() - about_window.winfo_width()) // 2
    y_position = (app.winfo_height() - about_window.winfo_height()) // 2
    about_window.geometry(f"+{app.winfo_x() + x_position}+{app.winfo_y() + y_position}")

    close_button = tk.Button(about_window, text="Close", command=about_window.destroy, font=custom_font, bg=bg_color, fg=fg_color)
    close_button.pack(pady=10)

def close_settings_window(settings_window):
    save_settings()
    settings_window.grab_release()
    settings_window.destroy()
    update_statusbar("Beállítások mentve.")
    app.after(3000, reset_statusbar)  # 5000 milliszekundum, azaz 5 másodperc után hívja meg a reset_statusbar függvényt

def save_settings():
    try:
        price = int(gomboc_price_entry.get())
        if price < 0:
            raise ValueError("Az ár nem lehet negatív.")
        gomboc_price.set(price)

        interval = int(free_gomboc_interval_entry.get())
        if interval <= 0:
            raise ValueError("Az intervallum nem lehet 0 vagy negatív.")
        free_gomboc_interval.set(interval)

        # Mentsük el a beállításokat a konfigurációs fájlba
        config['Settings']['gomboc_price'] = str(price)
        config['Settings']['free_gomboc_interval'] = str(interval)
        with open(config_file, 'w') as f:
            config.write(f)
    except ValueError as e:
        messagebox.showerror("Hiba", str(e))
        return False
    return True

# ...

def update_statusbar(message):
    status_label.config(text=message)
    status_label.update()

def reset_statusbar():
    status_label.config(text=f"Gombóc ár: {gomboc_price.get()} Ft, Ingyen gombóc, minden  {free_gomboc_interval.get()}.")


def calculate_price(gomboc_count):
    free_gomboc_interval_value = free_gomboc_interval.get()
    full_price_gombocs = gomboc_count - (gomboc_count // free_gomboc_interval_value)
    free_gombocs = gomboc_count // free_gomboc_interval_value
    total_price = full_price_gombocs * gomboc_price.get()
    return total_price, free_gombocs


def on_calculate():
    try:
        gomboc_count = int(gomboc_count_entry.get())
        if gomboc_count <= 0:
            raise ValueError("A gombócok száma pozitív egész szám kell legyen.")
        total_price, free_gombocs = calculate_price(gomboc_count)
        gomboc_count_entry.delete(0, tk.END)
        formatted_total_price = "{:,}".format(total_price).replace(",", " ")
        result_label.config(text=f"Gombócok száma: {gomboc_count}\nFizetendő: {formatted_total_price} Ft\nIngyen gombócok száma: {free_gombocs}")
    except ValueError:
        gomboc_count_entry.delete(0, tk.END)
        messagebox.showerror("Hiba", "A gombócok száma csak pozitív egész szám lehet.")

def create_menu():
    menu = tk.Menu(app)
    app.config(menu=menu)
    file_menu = tk.Menu(menu)
    menu.add_cascade(label="File", menu=file_menu)
    file_menu.add_command(label="Settings", command=open_settings_window)
    file_menu.add_command(label="About", command=open_about_window)
    file_menu.add_separator()
    file_menu.add_command(label="Exit", command=app.quit)

create_menu()

gomboc_count_label = tk.Label(app, text="Vásárolt gombócok száma:", font=custom_font, bg=bg_color, fg=fg_color)
gomboc_count_label.pack(pady=10)
gomboc_count_entry = tk.Entry(app, font=custom_font, width=8, justify='center')
gomboc_count_entry.pack(pady=5)

calculate_button = tk.Button(app, text="Számol", command=on_calculate, font=custom_font, bg=button_bg, fg=fg_color)
calculate_button.pack(pady=10)

result_label = tk.Label(app, font=custom_font, bg=bg_color, fg=fg_color)
result_label.pack(pady=10)

status_label = tk.Label(app, text=f"Aktuális ár: {gomboc_price.get()} Ft, Minden: {free_gomboc_interval.get()}. gombóc ajándék", bd=1, relief="sunken", anchor="center", font=custom_font, bg=bg_color, fg=fg_color)
status_label.pack(fill=tk.X, side=tk.BOTTOM, ipady=2)

app.bind('<Return>', lambda event: on_calculate())

#Config file definiálása,mentése, adatokkal feltöltése
import os
import configparser

config = configparser.ConfigParser()

# A konfigurációs fájl elérési útja
config_file = 'config.ini'

# Ha a konfigurációs fájl létezik, akkor betöltjük a beállításokat belőle
if os.path.isfile(config_file):
    config.read(config_file)
    gomboc_price.set(config['Settings']['gomboc_price'])
    free_gomboc_interval.set(config['Settings']['free_gomboc_interval'])
# Ha a konfigurációs fájl nem létezik, akkor létrehozzuk az alapértelmezett beállításokkal
else:
    config['Settings'] = {'gomboc_price': '450', 'free_gomboc_interval': '4'}
    with open(config_file, 'a') as f:
        config.write(f)

app.mainloop()
